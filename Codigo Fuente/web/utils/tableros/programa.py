import numpy as np
import pandas as pd
import json
import statistics
from itertools import chain 

import plotly.graph_objs as go
import plotly.express as px
from plotly.subplots import make_subplots

import os
from dotenv import load_dotenv
load_dotenv()
data_folder = os.getcwd()+os.getenv("DATA_FOLDER")

import logging
error_logger = logging.getLogger('error_logger')

#################################### VARIABLES GLOBALES #############################################

# Data source
df_IES = pd.read_excel(data_folder+'/ies.xlsx')
df_ESTUDIANTES = pd.read_excel(data_folder+'/estudiantes.xlsx')
periodos = [20101, 20102, 20111, 20112, 20121, 20122, 20131, 20132, 20141, 20142, 20151, 20152, 20161, 20162, 20171, 20172, 20181, 20182, 20191, 20192]
programas = df_IES.dropna(subset=['facultad','programa'], axis=0)['programa'].unique()

#################################### FUNCIONES GLOBALES #############################################

# Funcion para agregar un indicador a la figura 
def agregar_indicador(anterior,actual,fig,i,j,mode):
    if anterior == 0:
        anterior=actual/2
    fig.append_trace(go.Indicator(
        mode = mode,
        delta = {'reference': anterior,'relative': True, 'position' : "bottom"},
        value = actual,
        ),
        row=i,col=j
    )
    return fig

############################################################### INDICADORES PROGRAMA ####################################################################
def indicadores(PERIODO,PROGRAMA):
    try:
        # Dataframe del programa
        data_ies = df_IES.dropna(subset=['facultad'], axis=0)
        data_ies = df_IES.query("programa == '{}'".format(PROGRAMA))

        # Dataframe del periodo
        periodo_actual = int(PERIODO)
        data_actual = data_ies.query('periodo == {}'.format(periodo_actual))
        
        index_periodo_actual = periodos.index(periodo_actual)

        if 0 <= index_periodo_actual <= len(periodos):
            periodo_anterior = periodos[index_periodo_actual-1]
            data_anterior = data_ies.query('periodo == {}'.format( periodo_anterior))
        else:
            data_anterior = data_ies.query('periodo == {}'.format( periodo_actual))


        variables_nombre =  [ 'insc_total', 'admi_total','mat_total']
        variables_indicadores = []
        for v in variables_nombre:
            variables_indicadores.append([data_anterior[v].sum(), data_actual[v].sum()])
        cant_indicadores= len(variables_indicadores)

        # Crear conetenedor de sub graficos
        fig = make_subplots(
            column_titles=['Inscritos', 'Admitidos', 'Matriculados'],
            rows=1, cols=cant_indicadores,
            column_widths=[ 1/cant_indicadores ]*cant_indicadores,
            specs=[[{"type": "indicator"}]*cant_indicadores],
            horizontal_spacing = 0.2,
        )

        # Agregar cada indicador por variable 
        for i,var in enumerate(variables_indicadores):
            periodo_anterior = var[0]
            periodo_actual = var[1]
            fig = agregar_indicador(periodo_anterior,periodo_actual,fig,1,i+1,'number+delta')

        # Personalizar la grafica
        fig.update_layout(
            paper_bgcolor = '#fff',
            plot_bgcolor = '#fff',
            showlegend=False,
            margin=dict(l=0, r=15, b=0, t=30, pad=0),
        )

        return fig
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None

############################################################### RADIAL MATRICULA ####################################################################
def radial(PROGRAMA):
    try:
        # Dataframe del programa
        data_ies = df_IES.dropna(subset=['facultad'], axis=0)
        data_ies = df_IES.query("programa == '{}'".format(PROGRAMA))

        data = data_ies.dropna(subset=['periodo','mat_total'], axis=0)
        periodos = sorted(data['periodo'].unique())
        matriculas = []
        for p in periodos:
            mat = data.query('periodo == {}'.format(p))['mat_total'].values[0] 
            matriculas.append(mat)

        df_Matricula_Radial = pd.DataFrame({
            'periodo' : periodos,
            'matricula': matriculas,
            'teta': np.linspace(10,350,num=len(periodos))
        })
        azules = []
        i = 0
        for _ in periodos:
            if i == len(px.colors.sequential.Blues): 
                i=0
            azules.append(px.colors.sequential.Blues[i])
            i+=1

        fig = go.Figure(go.Barpolar(
            r=df_Matricula_Radial['matricula'],
            theta=df_Matricula_Radial['teta'],
            text = df_Matricula_Radial['periodo'],
            hovertext = df_Matricula_Radial['matricula'],
            hovertemplate ='<b>%{text}</b><br>Matricula: %{hovertext}<extra></extra>',
            width=[10]*len(periodos),
            marker_color=azules,
            marker_line_color="black",
            marker_line_width=2,
            opacity=0.8
        ))

        fig.update_layout(
            template=None,
            polar = dict(
                radialaxis = dict(
                    range=[0, max(matriculas)*1.03], 
                    showticklabels=False, 
                    ticks=''
                ),
                angularaxis = dict(
                    showticklabels=True, 
                    ticktext=periodos,
                    tickvals=df_Matricula_Radial['teta'],
                    ticks='inside',
                )
            ),
            margin=dict(l=0, r=0, b=30, t=30, pad=0),
        )
        return fig
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None

############################################################### PASTEL ESTRATOS ####################################################################
def pastel(PROGRAMA):
    try:
        # Dataframe del programa
        # TODO hacer el filtro por codigo o nombre de programa en la tabla VWDATADESERCION
        data = df_ESTUDIANTES.query("programa == '{}'".format(PROGRAMA))
        
        data = df_ESTUDIANTES['estrato_res']
        Estratos = sorted(data.unique())
        data = pd.DataFrame(data)
        Estudiantes = [len(data.query("estrato_res == {}".format(e))) for e in Estratos]
        Estratos = ['Estrato {}'.format(e) for e in Estratos]

        fig = px.pie(
            values=Estudiantes,
            names=Estratos,
            opacity=1,
            hole=.3,
            color_discrete_sequence=px.colors.sequential.ice[3:],
                    
            )
        # Personalizar la grafica
        fig.update_layout(
            paper_bgcolor = '#fff',
            plot_bgcolor = '#fff',
            showlegend=True,
            margin=dict(l=0, r=0, t=0, b=0,pad=0),
            legend={'traceorder':'reversed'}
        )
        fig.update_traces(
            hovertemplate= '<b>%{label}</b>'+
                '<br>%{value} <extra></extra>',
            marker=dict(line=dict(color='black', width=2)),
            pull=[0.07]*len(Estratos)
        )
        return fig
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None

############################################################### BARRAS DESERTORES ####################################################################
# Funcion para agregar un grafico por periodo
def barras_periodo(df,p,fig,i,azules):
    fig.append_trace(go.Bar(
        x=df['desert_int'],
        y=df['semestre'],
        marker_color=azules,
        marker=dict(
            # color= azules[i-1],
            line=dict(color='black', width=2)
        ),
        orientation='h',
        texttemplate='%{x}', 
        hovertemplate='<b>Periodo:</b> {}'.format(p)+'<br><b>Nivel:</b> %{y}<br><b>Desertor(es):</b> %{x}<extra></extra>', 
        textposition='outside',
        textfont=dict(
            size=30,
            color='rgb(186, 41, 41)'
        )
    ),row=1,col=i)
    if i == 1:
        fig.update_yaxes(dict(
        showgrid=False,
        showline=True, linewidth=2, linecolor='black',
        tickfont=dict(color='black', size=15),
        tickvals=df['semestre'],
        ticktext= ['Nivel <b>{}</b>               '.format(s) for s in df['semestre']],
        ),row=1,col=i)
    else:
        fig.update_yaxes(dict(
        showgrid=False,
        showline=True, linewidth=2, linecolor='black',
        showticklabels=False,
        ),row=1,col=i)
        
    fig.update_xaxes(dict(
        zeroline=False,
        showline=False,
        showticklabels=False,
        showgrid=False,
        range=[0,max(df['desert_int']*1.5)]
    ),row=1,col=i)

    return fig

def barras(PROGRAMA):
    try:
        # Dataframe del programa
        # TODO hacer el filtro por codigo o nombre de programa en la tabla VWDATADESERCION
        data = df_ESTUDIANTES.query("programa == '{}'".format(PROGRAMA))
        
        data = pd.DataFrame(df_ESTUDIANTES.loc[:,['semestre','periodo','desert_int']])
        data = data.groupby(['periodo','semestre'],as_index=False).sum()

        periodos = list(data['periodo'].unique())

        azules = []
        i = 0
        for _ in periodos:
            if i == len(px.colors.sequential.Blues): 
                i=0
            azules.append(px.colors.sequential.Blues[i])
            i+=1
        azules = list(reversed(azules))



        # Crear conetenedor de sub graficos
        fig = make_subplots(
            start_cell='top-left',
            column_titles= list(map(str,periodos)),
            rows=1, cols=len(periodos),
            # horizontal_spacing = 0.3,
        )
        # Recorrer los periodos y reliazar las graficas 
        for i,p in enumerate(periodos):
            data_periodo = data.query("periodo == {}".format(p))
            data_periodo = data_periodo.sort_values(by='semestre', ascending=True)
            data_periodo = data_periodo.head(10)
            
            fig = barras_periodo(data_periodo,p,fig,i+1,azules)

        
        fig.update_layout(
            plot_bgcolor = '#fff',
            paper_bgcolor = '#fff',
            bargap=0.2, 
            # bargroupgap=0.1,
            # title='<b>Desercion Niveles</b>',
            showlegend=False,
            margin=dict(l=0, r=0, b=15, t=30)
        )

        return fig

    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None    