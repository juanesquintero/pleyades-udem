import logging
import numpy as np
import pandas as pd
import json
import statistics
from itertools import chain

import plotly.graph_objs as go
import plotly.express as px
from plotly.subplots import make_subplots

import os
from dotenv import load_dotenv
load_dotenv()
data_folder = os.getcwd()+os.getenv("DATA_FOLDER")

error_logger = logging.getLogger('error_logger')

#################################### VARIABLES GLOBALES #############################################

# Data source
df_IES = pd.read_excel(data_folder+'/ies.xlsx')
df_ESTUDIANTES = pd.read_excel(data_folder+'/estudiantes.xlsx')
periodos = [20101, 20102, 20111, 20112, 20121, 20122, 20131, 20132, 20141,
            20142, 20151, 20152, 20161, 20162, 20171, 20172, 20181, 20182, 20191, 20192]
programas = df_IES.dropna(subset=['facultad', 'programa'], axis=0)[
    'programa'].unique()

colores = [' rgb(30, 75, 131)', 'rgb(26, 118, 255)',
           'rgb(186, 41, 41)', 'rgb(30, 59, 87)']

#################################### FUNCIONES GLOBALES #############################################


def estudiantes_programa(programa):
    try:
        # Dataframe del programa
        data = df_ESTUDIANTES.query("programa == '{}'".format(programa))
        # 'desertor', 'idprograma'
        data = data[['documento', 'nombre_completo']]
        estudiantes = data.drop_duplicates()
        return estudiantes.to_dict(orient='records')
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None


class Estudiante:

    def __init__(self, identificacion, programa=None,  periodo=None, ):
        self.identificacion = identificacion
        self.periodo = periodo
        self.programa = programa
        self.info = None
        self.periodos_estudiante = None
        self.programas_estudiante = None
        self.promedios_estudiante = None

    # Funcion para agregar un indicador a la figura
    def get_estudiante(self,):
        try:
            # Dataframe del programa
            data = df_ESTUDIANTES.query(
                "documento == '{}'".format(self.identificacion))
            self.programas_estudiante = list(data['programa'].unique())
            self.periodos_estudiante_todos = list(data['periodo'].unique())
            self.periodos_estudiante = list(data['periodo'].unique())
            
            
            # Filtrar Programa
            if self.programa:
                data = data.query("programa == '{}'".format(self.programa))
                self.periodos_estudiante = list(data['periodo'].unique())

            # Filtrar Periodo
            if self.periodo:
                data = data.query("periodo == '{}'".format(self.periodo))

            # Si hay Registro repetido
            if len(data) > 1:
                data = data[0:1]

            self.info = data.to_dict(orient='records')[0]

            # Setear perioodo y programa si no lo hay
            if not self.periodo:
                self.periodo = self.info['periodo']
            if not self.programa:
                self.programa = self.info['programa']

            return self.info, self.periodos_estudiante, self.programas_estudiante

        except Exception as e:
            error_logger.error('EXCEPTION: '+str(e), exc_info=True)
            return None, [], []

    def progreso_asignaturas(self,):
        try:
            asignaturas_totales = self.info['asig_plan']
            asignaturas_aprobadas = self.info['acum_asig_aprob']
            asignaturas_aprobadas_porcentaje = float(
                asignaturas_aprobadas/asignaturas_totales)
            fig = go.Figure(go.Bar(
                y=[''],
                x=[asignaturas_aprobadas],
                name='Asignaturas   ',
                marker_color=colores[1],
                marker=dict(
                    line=dict(color='black', width=2)
                ),
                orientation='h',
                texttemplate='   <b>{:.0%}</b>'.format(asignaturas_aprobadas_porcentaje),
                hovertemplate='<b>{}</b> aprobadas<br><b>{}</b> totales<extra></extra>'.format(
                    asignaturas_aprobadas, asignaturas_totales),
                textposition='outside',
                textfont=dict(
                    size=20,
                    color='black'
                ),
            ))

            fig.update_xaxes(dict(
                showline=True, linewidth=1, linecolor='black',
                showgrid=False,
                range=[0, asignaturas_totales]
            ))

            fig.update_layout(
                plot_bgcolor='#f5f5f5',
                paper_bgcolor='#fff',
                showlegend=False,
                margin=dict(l=0, r=0, b=15, t=10),
                font=dict(
                    size=8
                ),
            )
            return fig
        except Exception as e:
            error_logger.error('EXCEPTION: '+str(e), exc_info=True)

    def progreso_creditos(self,):
        try:
            creditos_totales = self.info['creditos_plan']
            creditos_aprobados = self.info['acum_cred_aprob']
            creditos_aprobados_porcentaje = creditos_aprobados/creditos_totales
            fig = go.Figure(go.Bar(
                y=[''],
                x=[creditos_aprobados],
                name='Créditos    ',
                marker_color=colores[0],
                marker=dict(
                    line=dict(color='black', width=2)
                ),
                orientation='h',
                texttemplate='   <b>{:.0%}</b>'.format(creditos_aprobados_porcentaje),
                hovertemplate='<b>{}</b> aprobados<br><b>{}</b> totales<extra></extra>'.format(
                    creditos_aprobados, creditos_totales),
                textposition='outside',
                textfont=dict(
                    size=20,
                    color='black'
                )
            ))

            fig.update_xaxes(dict(
                showline=True, linewidth=1, linecolor='black',
                showgrid=False,
                range=[0, creditos_totales]
            ))

            fig.update_layout(
                plot_bgcolor='#f5f5f5',
                paper_bgcolor='#fff',
                showlegend=False,
                margin=dict(l=0, r=0, b=15, t=10),
                font=dict(
                    size=8
                ),
            )

            return fig
        except Exception as e:
            error_logger.error('EXCEPTION: '+str(e), exc_info=True)

    def set_promedios(self,):
        try:
            # Dataframe del programa
            data = df_ESTUDIANTES.query(
                "documento == '{}'".format(self.identificacion))
            promedios = {}
            # for p in self.programas_estudiante:
            #     estu = data.query("programa == '{}'".format(p))
            #     estu = estu.sort_values(by=['periodo'], ascending=[True])
            #     promedios[p] = {
            #         # 'periodo': [str(e) for e in estu['periodo']],
            #         'periodo': list(estu['periodo']),
            #         'promedio': list(estu['prom_sem'])
            #     }
            estu = data.query("programa == '{}'".format(self.programa))
            estu = estu.sort_values(by=['periodo'], ascending=[True])
            promedios[self.programa] = {
                'periodo': list(estu['periodo']),
                'promedio': list(estu['prom_sem'])
            }
            self.promedios_estudiante = promedios
        except Exception as e:
            error_logger.error('EXCEPTION: '+str(e), exc_info=True)
            self.promedios_estudiante = {}

    def serie_promedio(self,):
        try:
            self.set_promedios()

            fig = make_subplots(specs=[[{"secondary_y": False}]])

            # Serie por promedios de un programa
            for i, programa in enumerate(self.promedios_estudiante.keys()):
                df = pd.DataFrame(self.promedios_estudiante[programa])
                fig.add_trace(go.Scatter(
                    x=df['periodo'],
                    y=df['promedio'],
                    name=programa,
                    mode='markers+lines+text',
                    text=list(df['promedio']),
                    textposition='top center',
                    texttemplate='%{text}',
                    textfont=dict(color=colores[i]),
                    marker=dict(
                        color=colores[i],
                    )
                ))

            fig.update_traces(
                hovertemplate='Promedio:   %{y:.2f}' +
                '<br>Período:      %{x} <extra></extra>',
                marker=dict(
                    size=10,
                    line=dict(width=1, color='white'),
                ),
            )
            fig.update_layout(
                font=dict(
                    size=10
                ),
                margin=dict(l=0, r=0, b=0, t=25, pad=0),
                showlegend=True,
                legend=dict(
                    x=0,
                    y=0,
                    bgcolor='rgba(255, 255, 255, 0)',
                    bordercolor='rgba(255, 255, 255, 0)'
                ),
                xaxis=dict(
                    title='Período',
                    showline=True, linewidth=2, linecolor='black', showgrid=True, gridwidth=1, gridcolor='#cccccc',
                    type='category', categoryorder='array', categoryarray=sorted(self.periodos_estudiante_todos)
                ),
                yaxis=dict(
                    title='Promedio',
                    showline=True, linewidth=2, linecolor='black',
                    showgrid=True, gridwidth=1, gridcolor='#cccccc', range=[0.0, 5.0]
                )
            )
            fig.layout.plot_bgcolor = '#fff'
            fig.layout.paper_bgcolor = '#fff'

            return fig
        except Exception as e:
            error_logger.error('EXCEPTION: '+str(e), exc_info=True)
            return None
