import pandas as pd
import warnings
import pickle
import logging
from ast import literal_eval
# Algoritmos comunes para clasificación
from sklearn import svm, tree, linear_model, neighbors, naive_bayes, ensemble, discriminant_analysis
# Apoyo para la validación cruzada
from sklearn import model_selection

error_logger = logging.getLogger('error_logger')


############################################################################################################# VERIFICACION DE DATOS CONJUNTO ##############################################################################################################
def verificar_data(data, periodoInicial, periodoFinal, programa):
    columnas = list(condiciones.keys())

    # Verificar si conjunto tiene columnas en str y la primera fila
    try:
        data.columns = map(str.lower, data.columns)
    except Exception as e:
        error_logger.error(e)
        return False, 'El conjunto no tiene columnas', None

    # Verificar si hay registros
    if not(len(data) > 0):
        return False, 'El conjunto no tiene registros (esta vacio)', None

    # Verificar Si todas las columnas existen
    if not(all(col in data.columns for col in columnas)):
        return False, 'El conjunto ingresado no posee las columnas requeridas', None
    if not(len(data.columns) == len(columnas)):
        return False, 'El conjunto ingresado tiene mas columnas de las requeridas', None
    # Verificar si los tipos de datos de colunma son correctos
    try:
        data_verificada = asignar_tipos(data)
    except Exception as e:
        error_logger.error(e)
        return False, 'El conjunto ingresado no tiene los tipos de dato por columna requeridos', None
    # Verificar si en conjunto posee mas de un  valor en la columna programa 
    if not(len(set(data_verificada['programa'].tolist()))==1):
        return False, 'El conjunto tiene resgistros de mas de un programa, los modelos se ejecutan por programa', None
    # Verificar si en conjunto posee los valores de periodo Inicial y Final Correctamente  
    if not(data_verificada['registro'].max() == periodoFinal):
        return False, 'El conjunto no tiene como periodo final {}, verifique los registros'.format(str(periodoFinal)), None
    if not(data_verificada['registro'].min() == periodoInicial):
        return False, 'El conjunto no tiene como periodo inicial {}, verifique los registros'.format(str(periodoInicial)), None    
    if not(data_verificada['idprograma'] == programa).all():
        return False, 'El conjunto no pertenece al programa indicado, verifique los registros', None    

    # Verificacion correcta
    return True, None, data_verificada


################################################################################################################ PREPARACION DE DATOS DE UN CONJUNTO ##############################################################################################################
def preparar_data(data):
    # Condiciones Precisas
    condiciones = [
        ('jornada', 'DIURNA'),
        ('genero', 'MASCULINO'),
        ('estado_civil', 'SOLTERO(A)'),
        ('trabaja', 'SI'),
        ('victima', 'SI'),
        ('pertenece_grupo_vulnerable', 'SI'),
        ('beca', 'SI'),
        ('intersemestral', 'SI'),
        ('desertor', 'SI'),
    ]
    for cond in condiciones:
        def func(row): return 1 if row[cond[0]] == cond[1] else 0
        data[cond[0]] = data.apply(func, axis=1)

    # Condiciones conjuntas 
    condiciones = [
        ('lugar_residencia_sede',  ['MEDELLIN', 'BELLO', 'ITAGUI','COPACABANA', 'ENVIGADO', 'SABANETA', 'BARBOSA', 'LA ESTRELLA'], 1, 0),
    ]
    for cond in condiciones:
        def func(row): return cond[2] if any(c in row[cond[0]] for c in cond[1]) else cond[3]
        data[cond[0]] = data.apply(func, axis=1)

    # Condiciones Especiales
    def func(row): return 0 if (row['etnia']=='NO APLICA' or row['etnia'] == None) else 1
    data['etnia'] = data.apply(func, axis=1)

    return data

############################################################################################################## EJECUCION DE MODELO CON UN CONJUNTO ##############################################################################################################

def eliminacion(data):
    
    warnings.filterwarnings('ignore')
    
    ''' FASE 1 '''
    data = data[data['semestre']>1]
    semestre_a_predecir = data['registro'].max()
    data_a_predecir = data[data['registro']>=semestre_a_predecir]
    data = data[data['registro']<semestre_a_predecir]

    columnas_eliminar_1 = [ 'registro','nombre_completo','tipo_documento','documento','trabaja','puntaje_icfes','beca','periodo_ingreso',
                            'ultimo_periodo','biologia','ciencias_naturales','ciencias_sociales','competencias_ciudadanas','filosofia',
                            'fisica','geografia','historia','ingles','lectura_critica', 'lenguaje','matematicas','quimica',
                            'razonamiento_cuantitativo','sociales_y_ciudadanas','programa','tipo_programa']
    data = data.drop(columnas_eliminar_1, axis=1)

    columnas_eliminar_nulos = [ 'semestre', 'jornada', 'genero', 'estado_civil', 'lugar_residencia_sede', 'etnia', 'victima',
                                'pertenece_grupo_vulnerable', 'promedio_acumulado','promedio_semestre', 'creditos_programa',  
                                'creditos_aprobados_sem', 'creditos_aprobados_acum','creditos_reprobados_sem', 'creditos_reprobados_acum',
                                'creditos_cancelados_sem', 'creditos_cancelados_acum', 'creditos_matriculados_sem','intersemestral','creditos_matriculados_acum']
    data.dropna(subset=columnas_eliminar_nulos, how='any',inplace=True) 
    data_a_predecir.dropna(subset=columnas_eliminar_nulos,how='any',inplace=True) 
    
    ''' FASE 2 '''
    x = data.groupby('semestre')['edad'].mean()

    for indice_fila, fila in data.loc[data.edad.isnull()].iterrows():
        data.loc[indice_fila,'edad'] = x[fila['semestre']]

    ''' FASE 3 '''
    columnas_eliminar_2 = ['asignaturas_aprobadas_acum','asignaturas_reprobadas_acum']
    data = data.drop(columnas_eliminar_2, axis=1)

    return data, data_a_predecir, semestre_a_predecir


def ejecutar_modelo(data):

    data, data_a_predecir,semestre_a_predecir =  eliminacion(data)

    ''' FASE 1 '''
    # Algoritmos de Machine Learning (AML)
    AML = [
    # Métodos Combinados
    ensemble.AdaBoostClassifier(),
    ensemble.BaggingClassifier(),
    ensemble.ExtraTreesClassifier(),
    ensemble.GradientBoostingClassifier(),
    ensemble.RandomForestClassifier(),

    # Modelo Lineal Generalizado (GLM)
    linear_model.LogisticRegressionCV(),
    linear_model.PassiveAggressiveClassifier(),
    linear_model.RidgeClassifierCV(),
    linear_model.SGDClassifier(),
    linear_model.Perceptron(),

    # Navies Bayes
    naive_bayes.BernoulliNB(),
    naive_bayes.GaussianNB(),

    # K-Nearest Neighbor
    neighbors.KNeighborsClassifier(),

    # Máquina de Vectores de Soporte (SVM)
    svm.SVC(probability=True),
    svm.LinearSVC(),

    # Árboles de Decisión
    tree.DecisionTreeClassifier(),
    tree.ExtraTreeClassifier(),

    # Análisis Discriminante (Discriminant Analysis)
    discriminant_analysis.LinearDiscriminantAnalysis(),
    discriminant_analysis.QuadraticDiscriminantAnalysis(),
    ]

    cv_split = model_selection.ShuffleSplit(n_splits = 10, test_size = .3, train_size = .6, random_state = 0 )
    AML_columns = ['Nombre', 'objeto','Parametros', 'Precision Media de Prueba', 'STD de la Precision * 3' ,'Tiempo']
    AML_compare = pd.DataFrame(columns = AML_columns)

    col_preparadas = ['semestre', 'jornada', 'edad', 'genero', 'estado_civil', 'lugar_residencia_sede', 'etnia', 'victima',
        'pertenece_grupo_vulnerable', 'promedio_semestre', 'creditos_programa',  'creditos_aprobados_sem', 'creditos_aprobados_acum',
        'asignaturas_aprobadas_sem', 'creditos_reprobados_sem', 'creditos_reprobados_acum', 'asignaturas_reprobadas_sem',
        'creditos_cancelados_sem', 'creditos_cancelados_acum', 'creditos_matriculados_sem', 'creditos_matriculados_acum',
        'promedio_acumulado', 'intersemestral']

    Target = ['desertor']
    AML_predict = data[Target]

    row_index = 0
    for alg in AML:

        AML_nombre = alg.__class__.__name__
        AML_compare.loc[row_index, 'Nombre'] = AML_nombre
        AML_compare.loc[row_index, 'Parametros'] = str(alg.get_params())
        cv_results = model_selection.cross_validate(alg, data[col_preparadas], data[Target], cv  = cv_split)

        AML_compare.loc[row_index, 'Precision Media de Prueba'] = cv_results['test_score'].mean()
        # Si es una muestra aleatoria sin sesgo, entonces la media +/- 3*(desviación estándar), deberían capturar el 99.7% de los subconjuntos
        AML_compare.loc[row_index, 'STD de la Precision * 3'] = cv_results['test_score'].std()*3
        AML_compare.loc[row_index, 'Tiempo'] = cv_results['fit_time'].mean()
        
        alg.fit(data[col_preparadas], data[Target])
        AML_predict[AML_nombre] = alg.predict(data[col_preparadas])
        AML_compare.loc[row_index, 'objeto'] = alg
        row_index+=1
        
    AML_compare.sort_values(by = ['Precision Media de Prueba'], ascending = False,inplace = True)

    ''' FASE 2 '''
    x = data_a_predecir.groupby('semestre')['edad'].mean()
    for indice_fila, fila in data_a_predecir.loc[data_a_predecir.edad.isnull()].iterrows():
        data_a_predecir.loc[indice_fila,'edad'] = x[fila['semestre']]

    ''' FASE 2 '''
    AML_best = AML_compare.head(1)
    mejor_clasificador = AML_best['objeto'].tolist()[0]
    
    # with open('clasificador.pkl', 'wb') as f: 
    #     pickle.dump(mejor_clasificador, f) 
    # pickle.dump(mejor_clasificador, open('clasificador.sav', 'wb'))

    # with open('clasificador.pkl', 'rb') as f: 
    #     clf = pickle.load(f)
    # clf = pickle.load(open('clasificador.sav', 'rb'))

    predc_sem_act = data_a_predecir[['documento','nombre_completo','desertor', 'idprograma']]
    
    predc_sem_act['prediccion'] = mejor_clasificador.predict(data_a_predecir[col_preparadas])

    potenciales_desertores = predc_sem_act[predc_sem_act['prediccion']==1]

    if(len(potenciales_desertores)!=0):
        # Eliminar valores repetidos
        potenciales_desertores = potenciales_desertores.drop_duplicates()

    # Setear resultados para insertar en la BD 
    potenciales_desertores['semestre_prediccion'] = semestre_a_predecir
    resultados_desertores = potenciales_desertores
    potenciales_desertores = potenciales_desertores.drop(['idprograma', 'semestre_prediccion'], axis=1)

    ''' FASE 3 '''
    pd.crosstab(predc_sem_act.prediccion,predc_sem_act.desertor,margins=True)
    total_desertores = len(potenciales_desertores.index)
    matriz_confusion = pd.crosstab(predc_sem_act.prediccion,predc_sem_act.desertor,margins=True)

    resultados ={
        'semestre_a_predecir': int(semestre_a_predecir),
        'desertores': potenciales_desertores,
        'total_desertores' : int(total_desertores), 
        'clasificador': str(AML_best['Nombre'].tolist()[0]),
        'precision': float(round(AML_best['Precision Media de Prueba'].tolist()[0]*100,2)),
    }
    
    return resultados, resultados_desertores

############################################################################################################### FUNCION DE ASIGNACION DE TIPOS DE DATOS CORRECTOS ###########################################################################################################################################

condiciones = {
    'registro':                                 int,
    'semestre':                                 int,
    'jornada':                                  str,
    'nombre_completo':                          str,
    'tipo_documento':                           str,
    'documento':                                str,
    'edad':                                     int,
    'genero':                                   str,
    'estado_civil':                             str,
    'lugar_residencia_sede':                    str,
    'trabaja':                                  str,
    'etnia':                                    str,
    'victima':                                  str,
    'pertenece_grupo_vulnerable':               str,
    'creditos_programa':                        int,
    'creditos_aprobados_sem':                   int,
    'creditos_aprobados_acum':                  int,
    'asignaturas_aprobadas_sem':                int,
    'asignaturas_aprobadas_acum':               int,
    'creditos_reprobados_sem':                  int,
    'creditos_reprobados_acum':                 int,
    'asignaturas_reprobadas_sem':               int,
    'asignaturas_reprobadas_acum':              int,
    'creditos_cancelados_sem':                  int,
    'creditos_cancelados_acum':                 int,
    'creditos_matriculados_sem':                int,
    'creditos_matriculados_acum':               int,
    'promedio_semestre':                        float,
    'promedio_acumulado':                       float,
    'puntaje_icfes':                            float,
    'beca':                                     str,
    'intersemestral':                           str,
    'desertor':                                 str,
    'periodo_ingreso':                          int,
    'ultimo_periodo':                           int,
    'biologia':                                 float,
    'ciencias_naturales':                       float,
    'ciencias_sociales':                        float,
    'competencias_ciudadanas':                  float,
    'filosofia':                                float,
    'fisica':                                   float,
    'geografia':                                float,
    'historia':                                 float,
    'ingles':                                   float,
    'lectura_critica':                          float,
    'lenguaje':                                 float,
    'matematicas':                              float,
    'quimica':                                  float,
    'razonamiento_cuantitativo':                float,
    'sociales_y_ciudadanas':                    float,
    'idmatricula':                              int,
    'idaspiracion':                             int,
    'idprograma':                               int,
    'programa':                                 str,
    'tipo_programa':                            str,
    'idfacultad':                               int,
    'facultad':                                 str,
}

def asignar_tipos(data):
    # # Cambiar valores NaN por None
    # data.replace([np.inf, -np.inf], None, inplace=True)
    # data.replace({np.nan: None}, inplace=True)

    # Asignar tipos de datos en cada columna
    for key, value in condiciones.items():
        # Obtener los valores de cada columna que no sean nulos 
        # y asignarle el tipo requerido para la columna  
        data[key][data[key].notna()] = data[key][data[key].notna()].astype(value)

    return data