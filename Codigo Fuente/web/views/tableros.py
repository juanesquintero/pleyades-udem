from flask import request, session, Blueprint, render_template, redirect, send_file, jsonify
import plotly
import json
import numpy as np
import os

from views.auth import login_required

# Importar tableros de plotly para cada nivel
import utils.tableros.mundo as Mundo
import utils.tableros.pais as Pais
import utils.tableros.region as Region
import utils.tableros.ies as IES
import utils.tableros.programa as Programa
import utils.tableros.estudiante as Estudiante_file
from utils.tableros.estudiante import Estudiante

endopoint = 'tableros/'

Tablero = Blueprint('Tablero', __name__)

niveles = [
    {'nombre': 'Nivel Mundo', 'ruta': '/mundo'},
    {'nombre': 'Nivel Pais', 'ruta': '/pais'},
    {'nombre': 'Nivel Region', 'ruta': '/region'},
    {'nombre': 'Nivel IES', 'ruta': '/institucion'},
    {'nombre': 'Nivel Programa', 'ruta': '/programa'},
    {'nombre': 'Nivel Estudiante', 'ruta': '/estudiante'},
]
periodos = np.arange(2010, 2019, 1)


@Tablero.route('/')
def menu():
    return render_template(endopoint+'index.html', niveles=niveles)


def to_plotly_json(fig):
    plt = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    return plt

############################################################## NIVEL MUNDO ################################################################
@Tablero.route('/mundo')
@login_required
def mundo_dashboard():
    # Series Por pais
    pais = request.args.get('pais')

    if pais:
        gastos, inscripciones = Mundo.series_pais(pais)
        gastos, inscripciones = to_plotly_json(
            gastos), to_plotly_json(inscripciones)
        return jsonify([gastos, inscripciones])
    else:
        gastos, inscripciones = Mundo.series_pais('COL')

    clusters = to_plotly_json(Mundo.clusters())
    mapa = to_plotly_json(Mundo.mapa())
    gastos, inscripciones = to_plotly_json(
        gastos), to_plotly_json(inscripciones)

    return render_template(
        endopoint+'mundial.html',
        gastos_plot=gastos,
        inscrp_plot=inscripciones,
        mapa_plot=mapa,
        clusters_plot=clusters
    )

############################################################## NIVEL PAIS ################################################################
@Tablero.route('/pais')
@login_required
def pais_dashboard():
    # Series Por pais
    periodo = request.args.get('periodo')
    if not periodo:
        periodo = '2018'

    mapa = to_plotly_json(Pais.mapa(periodo))
    barras = to_plotly_json(Pais.barras())
    pastel = to_plotly_json(Pais.pastel(periodo))
    genero = to_plotly_json(Pais.genero(periodo))
    indicadores = to_plotly_json(Pais.indicadores(periodo))
    indicadores2 = to_plotly_json(Pais.indicadores2(periodo))

    return render_template(
        endopoint+'nacional.html',
        mapa_plot=mapa,
        barras_plot=barras,
        pastel_plot=pastel,
        genero_plot=genero,
        indicadores_plot=indicadores,
        indicadores2_plot=indicadores2,
        periodos_list=periodos,
        periodo=int(periodo),
    )


############################################################## NIVEL REGION ################################################################
@Tablero.route('/region')
@login_required
def region_dashboard():

    periodo = request.args.get('periodo')
    dpto = request.args.get('dpto')

    if not periodo:
        periodo = '2018'
    if not dpto:
        dpto = 'ANTIOQUIA'

    dptos = Region.dptos

    # Mapa dpto
    mapa = Region.mapa(dpto, periodo)
    mapa = to_plotly_json(mapa) if mapa else None

    # Barras verticales retencion
    barras = Region.barras(dpto)
    barras = to_plotly_json(barras)if barras else None

    # Pastel sector IES matricula
    pastel = Region.pastel(dpto, periodo)
    pastel = to_plotly_json(pastel) if pastel else None

    # Barras genero matricula
    genero = Region.genero(dpto, periodo)
    genero = to_plotly_json(genero) if genero else None

    # Indicadores departamento
    indicadores_dpto = Region.indicadores_dpto(dpto, periodo)
    indicadores_dpto = to_plotly_json(
        indicadores_dpto) if indicadores_dpto else None

    # Indicadores y miniserie matricula
    indicadores_ies, cant_ies = Region.indicadores_ies(dpto)
    indicadores_ies = to_plotly_json(
        indicadores_ies) if indicadores_ies else None

    # Barras horizontales desercion (Solo Antioquia)
    barras_ies = Region.barras_ies(dpto, periodo)
    barras_ies = to_plotly_json(barras_ies) if barras_ies else None

    if cant_ies:
        if cant_ies < 10:
            ies_size = cant_ies*5*10
        else:
            ies_size = cant_ies*1.7*10
    else:
        ies_size = 0

    return render_template(
        endopoint+'regional.html',
        periodo=int(periodo),
        dpto=str(dpto),

        periodos_list=periodos,
        dptos_list=dptos,
        ies_size=ies_size,

        mapa_plot=mapa,
        barras_plot=barras,
        pastel_plot=pastel,
        genero_plot=genero,

        indicadores_dpto_plot=indicadores_dpto,
        indicadores_ies_plot=indicadores_ies,

        barras_ies_plot=barras_ies,
    )
############################################################## NIVEL IES ################################################################
@Tablero.route('/institucion')
@login_required
def ies_dashboard():
    periodo = request.args.get('periodo')

    if not periodo:
        periodo = '20192'

    periodos = IES.periodos

    # Indicadores IES
    indicadores1 = IES.indicadores1(periodo)
    indicadores1 = to_plotly_json(indicadores1) if indicadores1 else None

    indicadores2 = IES.indicadores2(periodo)
    indicadores2 = to_plotly_json(indicadores2) if indicadores2 else None

    # Barras facultades
    barras = IES.barras(periodo)
    barras = to_plotly_json(barras) if barras else None

    # Pastel facultades
    pastel = IES.pastel(periodo)
    pastel = to_plotly_json(pastel) if pastel else None

    # Lista Indicadores Programas
    indicadores_programas, cant_prgms_1 = IES.indicadores_programas(periodo)
    indicadores_programas = to_plotly_json(
        indicadores_programas) if indicadores_programas else None
    if cant_prgms_1:
        if cant_prgms_1 < 10:
            prgms_size_1 = cant_prgms_1*5*10
        else:
            prgms_size_1 = cant_prgms_1*1.7*10
    else:
        prgms_size_1 = 0

    # Lista Mini serie Programas
    miniseries_programas, cant_prgms_2 = IES.miniseries_programas()
    miniseries_programas = to_plotly_json(
        miniseries_programas) if miniseries_programas else None
    if cant_prgms_2:
        if cant_prgms_2 < 10:
            prgms_size_2 = cant_prgms_2*5*10
        else:
            prgms_size_2 = cant_prgms_2*1.7*10
    else:
        prgms_size_2 = 0

    return render_template(
        endopoint+'institucional.html',
        periodo=int(periodo),

        periodos_list=periodos,

        nombre_ies=os.getenv('CLI_IES_NAME'),

        prgms_size_1=prgms_size_1,
        prgms_size_2=prgms_size_2,

        indicadores_1_plot=indicadores1,
        indicadores_2_plot=indicadores2,

        barras_plot=barras,

        pastel_plot=pastel,

        indicadores_programas_plot=indicadores_programas,
        miniseries_programas_plot=miniseries_programas
    )

############################################################## NIVEL PROGRAMA ################################################################
@Tablero.route('/programa')
@login_required
def programa_dashboard():
    periodo = request.args.get('periodo')
    programa = request.args.get('programa')

    if not periodo:
        periodo = '20192'
    if not programa:
        programa = 'INGENIERÍA CIVIL'

    periodos = Programa.periodos
    programas = Programa.programas

    # Indicadores Programa
    indicadores = Programa.indicadores(periodo, programa)
    indicadores = to_plotly_json(indicadores) if indicadores else None

    # Radial Matricula
    radial = Programa.radial(programa)
    radial = to_plotly_json(radial) if radial else None

    # Pastel Estratos
    pastel = Programa.pastel(programa)
    pastel = to_plotly_json(pastel) if pastel else None

    # Barras Desertores
    barras = Programa.barras(programa)
    barras = to_plotly_json(barras) if barras else None

    return render_template(
        endopoint+'programa.html',
        periodo=int(periodo),
        programa=programa,
        # TODO diferencia el codigo del programa y el nombre
        nombre_programa=programa,
        nombre_ies=os.getenv('CLI_IES_NAME'),
        periodos_list=periodos,
        programas_list=programas,

        indicadores_plot=indicadores,
        radial_plot=radial,
        pastel_plot=pastel,
        barras_plot=barras,
    )


############################################################## NIVEL ESTUDIANTE ################################################################
@Tablero.route('/estudiante')
@login_required
def estudiante_dashboard():
    periodos = Estudiante_file.periodos
    programas = Estudiante_file.programas

    programa = request.args.get('programa')
    periodo = request.args.get('periodo')
    documento = request.args.get('documento')

    try:
        periodo = int(periodo)
    except Exception as e:
        periodo = None

    # Obtener la lista de estudiantes de un programa
    if programa and not documento:
        estudiantes_programa = Estudiante_file.estudiantes_programa(programa)
        return render_template(
            endopoint+'estudiantes_programa.html',
            estudiantes_list=estudiantes_programa,
            programa=programa,
            programas_list=programas,
        )
    
    # Buscar estudiante por programa o cedula
    if not documento:
        return render_template(
            endopoint+'buscar_estudiante.html',
            periodos_list=periodos,
            programas_list=programas,
        )

    # Obtener los graficos del estudiantes por documento identificacion
    estudiante = Estudiante(identificacion=documento,
                            programa=programa, periodo=periodo)

    info, periodos_estudiante, programas_estudiante = estudiante.get_estudiante()

    # Serie promedio acumulado
    serie_promedio = estudiante.serie_promedio()
    serie_promedio = to_plotly_json(serie_promedio) if serie_promedio else None

    # Progreso creditos
    creditos = estudiante.progreso_creditos()
    creditos = to_plotly_json(creditos) if creditos else None

    # Progreso asignaturas
    asignaturas = estudiante.progreso_asignaturas()
    asignaturas = to_plotly_json(asignaturas) if asignaturas else None

    # # Velocidad
    # velocidad = estudiante.velocidad()
    # velocidad = to_plotly_json(velocidad) if velocidad else None

    # Validar los periodos y programas
    if info:
        if not programa:
            programa = info['programa']
        if not periodo:
            periodo = int(info['periodo'])

    return render_template(
        endopoint+'estudiante.html',
        periodos_list=periodos_estudiante,
        programas_list=programas_estudiante,
        estudiante=info,
        serie_promedio_plot=serie_promedio,
        asignaturas_plot=asignaturas,
        creditos_plot=creditos,
        documento=documento,
        programa=programa,
        periodo=periodo,
        # velocidad_plot = velocidad,
    )
