from flask import request, session, Blueprint, render_template, redirect, send_file, url_for
from dotenv import load_dotenv
from datetime import datetime
from ast import literal_eval
from googletrans import Translator
import os, sys, json, pandas as pd

from services.db_cli import DB
from utils.modelo import preparar_data, verificar_data, ejecutar_modelo
from views.auth import login_required
from services.API import get, post, put, delete

from utils.utils import *

model_logger = logging.getLogger('model_logger')
error_logger = logging.getLogger('error_logger')

load_dotenv()

Conjunto = Blueprint('Conjunto', __name__)

endopoint = 'conjuntos/'

upload_folder = os.getcwd()+os.getenv("UPLOAD_FOLDER")

translator = Translator()

db = DB()

@Conjunto.route('/crudos')
@login_required
def crudos():
    return listar('crudos')

@Conjunto.route('/procesados')
@login_required
def procesados():
    return listar('procesados')
    
def listar(estado):
    status_p,body_p = get('programas')
    status_c,body_c = get('conjuntos/encargado/'+session['user']['correo']+'?estado='+estado)
    if status_c and status_p:
        return render_template(endopoint+estado+'.html', conjuntos=body_c, programas=body_p)        
    elif not(status_c) and not(status_p):
        error = {**body_c,**body_p}
    elif not(status_c):
        error = body_c
    else:
        error = body_p
    return render_template(endopoint+estado+'.html', conjuntos=[],error=error)

@Conjunto.route('/descargar/<estado>/<nombre>')
def descargar (estado, nombre):

    status_c,body_c = get('conjuntos/'+nombre)
    if not status_c:
        return render_template('utils/mensaje.html', mensaje='No existe ese conjunto')
    
    if estado.lower() == 'crudos':
        nombre = 'C '+nombre
    elif estado.lower() == 'procesados':
        nombre = 'P '+nombre
    else:
        return render_template('utils/mensaje.html', mensaje='Estado del conjunto incorrecto')

    ruta = upload_folder+'/'+estado.lower()+'/'+nombre
    
    if os.path.exists(ruta+'.xlsx'):
        return send_file(ruta+'.xlsx', as_attachment=True)
    elif os.path.exists(ruta+'.xls'):
        return send_file(ruta+'.xls', as_attachment=True)
    else:
        return render_template('utils/mensaje.html', mensaje='No se encontro el archivo a descargar')    
    

@Conjunto.route('/crear')
@login_required
def crear():
    status_f,body_f = get('facultades')
    status_p,body_p = get('programas')

    if status_f and status_p:
        return render_template(endopoint+'crear.html', facultades=body_f, programas=body_p)
    elif not(status_f) and not(status_p):
        error = {**body_f,**body_p}
    elif not(status_f):
        error = body_f
    else:
        error = body_p
    return render_template('utils/mensaje.html', mensaje='No se pudieron cargar las programas y las facultades', submensaje=error)

@Conjunto.route('/detalle',methods=['POST'])
@login_required
def detalle():
    # Obtener Lo valores del formulario
    body = dict(request.values)
    conjunto = literal_eval(body['conjunto'])
    # Consultas para mostrar info
    status_f,body_f = get('facultades')
    status_p,body_p = get('programas')
    status_u, body_u = get('usuarios')
    
    if status_p and status_f and status_u and conjunto:
        return render_template(endopoint+'detalle.html', facultades=body_f, programas=body_p, usuarios=body_u, c=conjunto)
    elif not(status_f) and not(status_p) and not(status_u):
        error = {**body_f,**body_p,**body_u}
    elif not(status_f):
        error = body_f
    elif not(status_p):
        error = body_p
    elif not(conjunto):
        return render_template('utils/mensaje.html', mensaje='No se encontro un conjunto para detallar')
    else:
        error = body_u
    return render_template('utils/mensaje.html', mensaje='No se pudieron cargar los datos para detallar el conjunto', submensaje=error)

@Conjunto.route('/crear', methods=['POST'])
@login_required
def guardar():
    # Obtener Lo valores del formulario
    conjunto = dict(request.values)
    # Preparar conjunto para la insercion
    del conjunto['facultad']
    conjunto['estado'] = 'Crudos'
    conjunto['periodoInicial'] = int(conjunto['periodoInicial'])
    conjunto['periodoFinal'] = int(conjunto['periodoFinal'])
    conjunto['programa'] = int(conjunto['programa'])
    conjunto['encargado'] = session['user']['correo']

    # Obtener nombre del conjunto desde el api
    status_n, body_n = post('conjuntos/nombre', conjunto)
    if status_n:
        nombre, numero = body_n['nombre'], body_n['numero']
    else:
        return render_template('utils/mensaje.html', mensaje='No se pudo obtener el nombre del conjunto', submensaje=body_n)

    archivo_guardar = 'C '+ nombre
    tipo = conjunto['tipo']
    ruta = upload_folder+'/crudos'
    archivo = request.files['archivo']

    # Guardar archivo en Upload folder

    ############# ARCHIVO ##############
    if (archivo.filename and tipo=='excel'):
        extension = '.'+archivo.filename.split('.')[1]
        # Guardar archivo de excel
        archivo_guardar = archivo_guardar + '.xls'
        if not (extension in ['.xls', '.xlsx']):
            return render_template('utils/mensaje.html', mensaje='Extension de archivo incorrecta: '+str(extension), submensaje='Solo se permiten archivos excel .xls & xlsx')
    
        # VERIFICACION de formato
        data = pd.read_excel(archivo)
        validacion, mensaje_error, data_verificada = verificar_data(data, conjunto['periodoInicial'], conjunto['periodoFinal'], conjunto['programa'])
        if validacion:
            try:
                data_verificada.to_excel(ruta+'/'+archivo_guardar, index=False)
                # archivo.save(os.path.join(ruta, nombre + extension))
            except Exception as e:
                error_logger.error(e)
                return render_template('utils/mensaje.html', mensaje='Ocurrio un error guardando el conjunto de datos')
        else:
            return render_template('utils/mensaje.html', mensaje='Incorrecto el formato de la fuente de datos', submensaje=mensaje_error)
    
    ############# CONSULTA ##############
    elif tipo=='consulta':
        # Obtener datos desde la bd SQL server       
        sql = "SELECT * FROM VWDATADESERCION WHERE idprograma={} AND registro >= {} AND registro <= {} "
        sentencia = sql.format(conjunto['programa'], conjunto['periodoInicial'], conjunto['periodoFinal'])
        data = db.select(sentencia)
        ex = exception(data)
        if ex: return ex
        
        # VERIFICACION de formato
        validacion, mensaje_error, data_verificada = verificar_data(data, conjunto['periodoInicial'], conjunto['periodoFinal'], conjunto['programa'])
        if validacion:
            # Guardar tabla sql como excel
            try:
                data_verificada.to_excel(ruta+'/'+archivo_guardar+'.xls', index=False)
            except Exception as e:
                error_logger.error(e)
                return render_template('utils/mensaje.html', mensaje='Ocurrio un error guardando el conjunto de datos')
        else:
            return render_template('utils/mensaje.html', mensaje='Incorrecto el formato de la fuente de datos', submensaje=mensaje_error)
    else:
        return render_template('utils/mensaje.html', mensaje='Formulario incorrecto', submensaje='Verifica el formulario de creacion o notifica al Administrador del sistema')
           
    # Guardar registro de conjunto en la BD
    conjunto['nombre'] =  nombre
    conjunto['numero'] =  numero
    status, body = post('conjuntos',conjunto)
    if status:
        return redirect(url_for('Conjunto.crudos'))
    else:
        return render_template('utils/mensaje.html', mensaje='No se pudo guardar el conjunto', submensaje=body)


@Conjunto.route('/preparar', methods=['POST'])
@login_required
def preparar():
    # Obtener Lo valores del formulario
    body = dict(request.values)
    conjunto = literal_eval(body['conjunto'])
    
    nombre = conjunto['nombre']

    # Actualizar conjunto de datos de crudo a en proceso 
    act_estado =  actualizar_estado(nombre,'En Proceso')
    if act_estado: return act_estado

    # Crear prepraracion
    preparacion = {}
    preparacion['conjunto'] = conjunto['nombre']
    preparacion['preparador'] = session['user']['correo']
    preparacion['fechaInicial'] = getNowDate()
    # Obtener numero de preparacion para el conjunto
    status_p, body_p = get('preparaciones/nombre/'+nombre)
    if status_p:
        preparacion['numero'] = body_p['numero']
        preparacion['nombre'] = body_p['nombre']
    else:
        return render_template('utils/mensaje.html', mensaje='No se pudo obtener el consecutivo de la preparacion para este conjunto', submensaje=body_p)

    ########### PREPARAR ############

    # Obtener archivo crudo
    archivo_crudo = 'C '+nombre 
    ruta = upload_folder+'/crudos/'+archivo_crudo
    exito,dataCruda = obtener_archivo_excel(ruta) 
    if not(exito): return dataCruda 

    # Algoritmo de preparacion
    try:
        dataPreparada = preparar_data(dataCruda)
    except Exception as e:
        model_logger.error(e)
        observaciones = {'error': str(e)}
        exito,pagina_error = guardar_preparacion(preparacion, observaciones,'Fallida')
        if not(exito): return pagina_error 
        return render_template('utils/mensaje.html', mensaje='La preparacion Fallo')

    # Guardar archivo en Upload folder procesados
    archivo_procesado = 'P '+nombre+'.xls'
    ruta = upload_folder+'/procesados/'+archivo_procesado
    exito,pagina_error = guardar_archivo(dataPreparada, ruta, 'excel')
    if not(exito): return pagina_error 

    # Guardar registro de preparacion en la BD 
    exito, pagina_error = guardar_preparacion(preparacion,None,'Exitosa')
    if not(exito): return pagina_error 

    ########### FIN PREPARAR ############    

    # Actualizar conjunto de datos de crudo a procesado
    act_estado =  actualizar_estado(nombre,'Procesados')
    if act_estado: return act_estado

    return redirect(url_for('Conjunto.procesados'))


@Conjunto.route('/ejecutar', methods=['POST'])
@login_required
def ejecutar():
    # Obtener Lo valores del formulario
    body = dict(request.values)
    conjunto = literal_eval(body['conjunto'])
    nombre = conjunto['nombre']

    # Actualizar conjunto de datos de crudo a procesado
    act_estado =  actualizar_estado(nombre,'En Proceso')
    if act_estado: return act_estado
    
    
    # Crear prepraracion
    ejecucion = {}
    ejecucion['conjunto'] = conjunto['nombre']
    ejecucion['ejecutor'] = session['user']['correo']
    ejecucion['fechaInicial'] = getNowDate()

    # Obtener numero de ejecucion para el conjunto
    status_p, body_p = get('ejecuciones/nombre/'+nombre)
    if status_p:
        ejecucion['nombre'] = body_p['nombre']
        ejecucion['numero'] = body_p['numero']
    else:
        return render_template('utils/mensaje.html', mensaje='No se pudo obtener el consecutivo de la preparacion para este conjunto', submensaje=body_p)

    ########### EJECUTAR ############

    # Obtener archivo procesado
    archivo_procesado = 'P '+nombre 
    ruta = upload_folder+'/procesados/'+archivo_procesado
    exito,dataPreparada = obtener_archivo_excel(ruta) 
    if not(exito): return dataPreparada 
    
    # Algoritmo de ejecucion
    # try:
    #     resultados_modelo,resultados_desertores = clasificador(dataPreparada)
    # except Exception as e:
    #   pass
    try:
        resultados_modelo,resultados_desertores = ejecutar_modelo(dataPreparada)
    except Exception as e:
        model_logger.error(e)
        error_spa = translator.translate(str(e), src='en', dest='es').text
        resultados = {'error': error_spa }
        exito,pagina_error = guardar_ejecucion(ejecucion, resultados,'Fallida')
        if not(exito): return pagina_error 
        act_estado =  actualizar_estado(nombre,'Procesados')
        if act_estado: return act_estado
        return render_template('utils/mensaje.html', mensaje='La ejecucion Fallo', submensaje=error_spa)
        
        


    # Guardar resultados de desertotres en Upload folder desertores
    archivo_desertores = 'D '+ejecucion['nombre']+'.json'
    ruta = upload_folder+'/desertores/'+archivo_desertores
    exito,pagina_error = guardar_archivo(resultados_modelo.pop('desertores'), ruta, 'json')
    if not(exito): return pagina_error 

    # Guardar registro de los desertores en la BD del cli
     
    update_result_table = db.sql('UPDATE TBLDES_RESULTADO_PREDICCION SET blnultimo=0 WHERE semestre_prediccion={} AND idprograma={};'.format(str(conjunto['periodoFinal']),str(conjunto['programa'])))
    ex = exception(update_result_table)
    if ex:
        act_estado =  actualizar_estado(nombre,'Procesados')
        if act_estado: return act_estado 
        return ex
    
    insert_desertores = db.insert(resultados_desertores,'TBLDES_RESULTADO_PREDICCION')
    ex = exception(insert_desertores)
    if ex: 
        act_estado =  actualizar_estado(nombre,'Procesados')
        if act_estado: return act_estado
        return ex

    # Guardar registro de ejecucion en la BD 
    exito, pagina_error = guardar_ejecucion(ejecucion, resultados_modelo ,'Exitosa')
    if not(exito): 
        act_estado =  actualizar_estado(nombre,'Procesados')
        if act_estado: return act_estado
        return pagina_error 

    ########### FIN EJECUTAR ############
    # Actualizar conjunto de datos de crudo a procesado 
    act_estado =  actualizar_estado(nombre,'Procesados')
    if act_estado: return act_estado
    return redirect(url_for('Resultado.ejecuciones',conjunto=nombre))