from flask import request, jsonify, Blueprint
from db.cli.db_cli import DB
from schemas.facultadSchema import validate_post_schema, validate_put_schema
from flask_jwt_extended import jwt_required
from utils.utils import *
import sys

Facultad = Blueprint('facultad', __name__)

db = DB()

@Facultad.route('/')
@jwt_required
def get():
    query = db.select("SELECT * FROM VWFACULTADDESERCION;")
    ex = exception(query)
    if ex: 
        return ex
    if not(query): 
        return {'msg': "No hay Facultades"}, 404
    return jsonify(format(query))


@Facultad.route('/<codigo>')
@jwt_required
def getOne(codigo):
    query = db.select("SELECT * FROM VWFACULTADDESERCION WHERE codigo={};".format(codigo))
    ex = exception(query)
    if ex: 
        return ex
    if not(query): 
        return {'msg': "No hay concidencias"}, 404
    return jsonify(format(query)[0])


@Facultad.route('',methods=['POST'])
@jwt_required
def post():
    body = request.get_json()
    # validate schema
    if not(validate_post_schema(body)):
        return {'error': "body invalido"}, 400
    # sql validations
    lista = db.select("SELECT * FROM VWFACULTADDESERCION;")
    for f in lista:
        if f['codigo']==body['codigo']:
            return {'error': "codigo ya existe"}, 400
        if f['nombre']==body['nombre']:
            return {'error': "nombre ya existe"}, 400
    # Insert 
    insert = db.insert(body,'VWFACULTADDESERCION')
    ex = exception(insert)
    if ex: 
        return ex
    return {'msg': "facultad creada"}, 200

@Facultad.route('/',methods=['POST'])
@jwt_required
def post2():
    return post()

@Facultad.route('/<codigo>',methods=['PUT'])
@jwt_required
def put(codigo):
    body = request.get_json()
    if not(codigo):
        return {'error': "indique el codigo por el path"}, 404
    # validate schema
    if not(validate_put_schema(body)):
        return {'error': "body invalido"}, 400
    # sql validations
    if not exists(codigo):  return {'error': "facultad no existe"}, 404
    # Uptade 
    condicion="codigo="+str(codigo)
    update = db.update(body,condicion,'VWFACULTADDESERCION')
    ex = exception(update)
    if ex: 
        return ex
    return {'msg': "facultad actualizada"}, 200

@Facultad.route('/<codigo>',methods=['DELETE'])
@jwt_required
def deleteOne(codigo):
    if not(codigo):
        return {'error': "indique el codigo por el path"}, 404
    # sql validations
    if not exists(codigo):  
        return {'error': "facultad no existe"}, 404
    # delete 
    condicion="codigo="+str(codigo)
    delete = db.delete(condicion,'VWFACULTADDESERCION')
    ex = exception(delete) 
    if ex: 
        return ex
    return {'msg': "facultad eliminada"}, 200
    
def exists(codigo):
    codigo = int(codigo)
    query = db.select("SELECT * FROM VWFACULTADDESERCION;")
    if exception(query): 
        return False
    lista = map(lambda f : f['codigo'], query) 
    return True if codigo in lista else False


def format(query):
    for q in query:
        q['codigo'] = int(q['codigo'])
    return query