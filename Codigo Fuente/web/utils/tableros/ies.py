import numpy as np
import pandas as pd
import json
import statistics
from itertools import chain 

import plotly.graph_objs as go
import plotly.express as px
from plotly.subplots import make_subplots

import os
from dotenv import load_dotenv
load_dotenv()
data_folder = os.getcwd()+os.getenv("DATA_FOLDER")

import logging
error_logger = logging.getLogger('error_logger')

#################################### VARIABLES GLOBALES #############################################

# Data source
df_IES = pd.read_excel(data_folder+'/ies.xlsx')
periodos = [20101, 20102, 20111, 20112, 20121, 20122, 20131, 20132, 20141, 20142, 20151, 20152, 20161, 20162, 20171, 20172, 20181, 20182, 20191, 20192]

#################################### FUNCIONES GLOBALES #############################################

# Funcion para agregar un indicador a la figura 
def agregar_indicador(anterior,actual,fig,i,j,mode):
    if anterior == 0:
        anterior=actual/2
    fig.append_trace(go.Indicator(
        mode = mode,
        delta = {'reference': anterior,'relative': True, 'position' : "bottom"},
        value = actual,
        ),
        row=i,col=j
    )
    return fig

############################################################### INDICADORES IES ####################################################################
def indicadores1(periodo):
    try:
        # Dataframe del periodo
        periodo_actual = int(periodo)
        data = df_IES.dropna(subset=['facultad'], axis=0)
        data_actual = data.query('periodo == {}'.format(periodo_actual))
        data = df_IES.query("programa == 'TOTAL GENERAL'")

        index_periodo_actual = periodos.index(periodo_actual)
        
        if 0 <= index_periodo_actual <= len(periodos):
            periodo_anterior = periodos[index_periodo_actual-1]
            data_anterior = data.query('periodo == {}'.format( periodo_anterior))
        else:
            data_anterior = data.query('periodo == {}'.format( periodo_actual))

        # Variables indicadores
        # variables_nombre =  [ 'insc_total', 'admi_total','matriculados']
        # variables_indicadores = []
        # for v in variables_nombre:
        #    variables_indicadores.append([data_anterior[v].sum(), data_actual[v].sum()])
        # cant_indicadores= len(variables_indicadores)

        variables_nombre =  [ 'insc_total', 'admi_total','mat_total']
        variables_indicadores = []
        for v in variables_nombre:
            variables_indicadores.append([data_anterior[v].sum(), data_actual[v].sum()])
        
        cant_indicadores= len(variables_indicadores)

        # Crear conetenedor de sub graficos
        fig = make_subplots(
            column_titles=['Inscritos', 'Admitidos', 'Matriculados'],
            rows=1, cols=cant_indicadores,
            column_widths=[ 1/cant_indicadores*0.5 ]*cant_indicadores,
            specs=[[{"type": "indicator"}]*cant_indicadores],
            # horizontal_spacing = 0.18,
        )

        # Agregar cada indicador por variable 
        for j,var in enumerate(variables_indicadores):
            periodo_anterior = var[0]
            periodo_actual = var[1]
            fig = agregar_indicador(periodo_anterior,periodo_actual,fig,1,j+1,'number+delta')

        # Personalizar la grafica
        fig.update_layout(
            paper_bgcolor = '#fff',
            plot_bgcolor = '#fff',
            showlegend=False,
            margin=dict(l=0, t=30, r=0, b=0),
        )
        return fig
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None

def datos_periodo(periodo):
    # Eliminar el Total General
    data = df_IES.dropna(subset=['facultad'], axis=0)

    # Filtrar por periodo 
    data = data.query('periodo == {}'.format(periodo))

    # Eliminar valores nulos en para ese periodo 
    data = data.dropna(subset=['desertores', 'desercion', 'mat_total'], axis=0)
    data = data.query('desercion != 0 & desertores != 0 & mat_total != 0')
    list_programas = data['programa'].unique()

    # df = df.replace(0, np.nan)
    # df = df.dropna(how='all', axis=0)
    # df = df.replace(np.nan, 0)

    # Obtener valores para ese periodo
    programas = len(list_programas)
    egresados = data['egresados'].sum()

    return programas, egresados

def indicadores2(periodo):
    try:
        # Dataframe del periodo
        periodo_actual = int(periodo)
        data = df_IES.dropna(subset=['facultad'], axis=0)
        data_actual = data.query('periodo == {}'.format(periodo_actual))

        index_periodo_actual = periodos.index(periodo_actual)
        
        if 0 <= index_periodo_actual <= len(periodos):
            periodo_anterior = periodos[index_periodo_actual-1]
            data_anterior = data.query('periodo == {}'.format( periodo_anterior))
        else:
            data_anterior = data.query('periodo == {}'.format( periodo_actual))

        # Crear conetenedor de sub graficos
        fig = make_subplots(
            start_cell='top-left',
            column_titles= [ 'Programas','Egresados'],
            rows=1, cols=2,
            column_widths=[0.5,0.5],
            specs=[[{"type": "indicator"},{"type": "indicator"}]],
        )
        
        programas_periodo_actual, egresados_periodo_actual  = datos_periodo(periodo_actual)
        programas_periodo_anterior, egresados_periodo_anterior = datos_periodo(periodo_anterior)

        fig = agregar_indicador(programas_periodo_anterior,programas_periodo_actual,fig,1,1,'number')
        fig = agregar_indicador(egresados_periodo_anterior,egresados_periodo_actual,fig,1,2,'number')

        # Personalizar la grafica
        fig.update_layout(
            paper_bgcolor = '#fff',
            plot_bgcolor = '#fff',
            showlegend=False,
            margin=dict(l=0, t=30, r=0, b=0),
        )
        return fig
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None

############################################################### BARRAS ####################################################################
def barras(periodo):
    try:
        periodo_actual = int(periodo)
        # Agrupar por Facultades
        data_facultades = df_IES.query('periodo == {}'.format(periodo_actual))
        data_facultades = data_facultades.dropna(subset=['facultad'], axis=0)

        data_facultades= data_facultades[['facultad','programa','programa_nombre_corto','mat_total','admi_total','insc_total','mat_nuevos_total']]
        data_facultades = data_facultades.groupby(['facultad'],as_index=False).sum()
        data_facultades =  data_facultades.sort_values(by=['admi_total','insc_total','mat_nuevos_total'], ascending=[True,True,True])

        # Create figure with secondary y-axis
        fig = make_subplots(specs=[[{"secondary_y": True}]])

        # BARRAS
        # Inscritos
        fig.add_trace(go.Bar(
            x=data_facultades['facultad'],
            y=data_facultades['insc_total'],
            name='Inscritos',
            marker=dict(
            line=dict(color='#000000', width=2),
            color='rgb(55, 83, 109)',
            opacity=0.9
            )
        ),
        secondary_y=False)
        # Admitidos
        fig.add_trace(go.Bar(
            x=data_facultades['facultad'],
            y=data_facultades['admi_total'],
            name='Admitidos',
            marker=dict(
                line=dict(color='#000000', width=2),
                color='rgb(26, 118, 255)',
                opacity=0.9
            )
        ),
        secondary_y=False)
        # Matriculados
        fig.add_trace(go.Bar(
            x=data_facultades['facultad'],
            y=data_facultades['mat_nuevos_total'],
            name='Matriculados',
            marker=dict(
                line=dict(color='#000000', width=2),
                color='rgb(186, 41, 41)',
                opacity=0.9
            )
        ),
        secondary_y=False)
        # SERIES
        # Tasa Admitidos/Matriculados
        tasa = []
        for a,m in zip(data_facultades['admi_total'],data_facultades['mat_nuevos_total']):
            try:
                tasa.append(round(a/m*100,1))
            except:
                tasa.append(0)

        fig.add_trace(go.Scatter(
            x=data_facultades['facultad'],
            y=tasa,
            name='Tasa de absorcion',
            mode='markers+lines+text',
            text=tasa,
            textposition='top center',
            texttemplate='%{text} %',
            textfont=dict(color='rgb(186, 41, 41)'),
            marker=dict(
                color='rgb(186, 41, 41)',
            ),
        ),
        secondary_y=True)

        # Personalizar figura
        fig.update_layout(
            plot_bgcolor = '#fff',
            paper_bgcolor = '#fff',
            xaxis=dict(
                title='Facultades', 
                # tickfont_size=14,
                showticklabels=False, 
                showgrid=True, 
                gridwidth=1, 
                gridcolor='#fff',
                showline=True, linewidth=2, linecolor='black'
            ),
            yaxis=dict(
                title='Cantidad de Estudiantes',
                # titlefont_size=16,
                # tickfont_size=14,
                showgrid=True, gridwidth=1, gridcolor='#fff',
                showline=True, linewidth=2, linecolor='black',
                range=[0, max(chain(data_facultades['mat_nuevos_total'],data_facultades['insc_total'],data_facultades['admi_total']))*1.4],
            ),
            legend=dict(
                # x=0,
                # y=1,
                bgcolor='rgba(255, 255, 255, 0)',
                bordercolor='rgba(255, 255, 255, 0)'
            ),
            barmode='group',
            bargap=0.25, # gap between bars of adjacent location coordinates.
            bargroupgap=0, # gap between bars of the same location coordinate.
            margin=dict(l=0, t=0, r=0, b=0),
        )

        fig.update_yaxes(
            showline=False,
            showgrid=False,
            showticklabels=False, 
            title='',
            fixedrange=True, 
            range=[max(tasa)*2*-1, max(tasa)*2.5],
            secondary_y=True)

        fig.update_traces(
            hovertemplate= '<b>%{x}</b>'+
                            '<br>%{y} <extra></extra>',
            # hoverlabel = dict(bgcolor='white'),
        )
        return fig
    except  Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None

############################################################### LISTA INDICADORES PROGRAMAS ####################################################################
def crear_indicador(data,variable,i,j,fig,tipo):
    # Indicador 
    periodo_anterior = data.loc[len(data)-2,variable]*100 if len(data)-2 > 0 else None
    periodo_actual = data.loc[len(data)-1,variable]*100 if len(data)-1 > 0 else None
    
    # if periodo_actual == periodo_anterior:
    #     periodo_actual = None
    #     periodo_anterior = None

    if periodo_anterior == 0 :
        periodo_anterior = periodo_actual/2
    
    fig.append_trace(go.Indicator(
        mode = tipo,
        delta = {'reference': periodo_anterior,'relative': True, 'position' : "bottom"},
        value = periodo_actual,
        ),
        row=i,col=j
    )
    return fig

# Funcion para agregar un registro de programas a la lista de graficos
def indicadores_programa_row(data,fig,i):
    x = [0]
    # Nombre Programa
    fig.append_trace(go.Scatter(
        x=x, 
        y=x,
        hoverinfo='skip',
        marker=dict(
            color='#fff'  
        ),
        ),row=i,col=1)
    fig.update_yaxes(
        showline=False,
        tickvals=x,
        ticktext = ['<b>'+data['programa_nombre_corto'][0]+'</b>'],
        tickfont=dict(color='black'),
        fixedrange=True,
        row=i, col=1
        ) 
    fig.update_xaxes(
        showline=False,
        showticklabels=False,
        title="",
        fixedrange=True,
        row=i, col=1
    )
    
    fig = crear_indicador(data,'mat_hombre',i,2,fig,'number+delta')
    fig = crear_indicador(data,'mat_mujer',i,3,fig,'number+delta')
    fig = crear_indicador(data,'mat_total',i,4,fig,'number+delta')
    fig = crear_indicador(data,'desercion',i,5,fig,'number+delta')

    return fig


def indicadores_programas(periodo):
    try:
        periodo_actual = int(periodo)
        
        # Programas
        data = df_IES.dropna(subset=['facultad'], axis=0)
        data = data.sort_values(by=['periodo','mat_total'],ascending=[False,False])
        # data = data.sort_values(by=['mat_total'],ascending=False)
        list_programas = data['programa'].unique()
        cant_programas = len(list_programas)

        # Crear conetenedor de sub graficos
        fig = make_subplots(
            start_cell='top-left',
            column_titles=['','<b>Mat. Hombres</b>','<b>Mat. Mujeres</b>','<b>Mat. Total</b>','<b>Desercion</b>'],
            rows=cant_programas, cols=5,
            column_widths=[0,0.25,0.25,0.25,0.25],
            specs=[[{"type": "scatter"},{"type": "indicator"},{"type": "indicator"}, {"type": "indicator"},{"type": "indicator"}]]*cant_programas,
        )


        # df_IES_periodo= df_IES.query('periodo == {} | periodo == {}'.format(periodo_anterior,periodo_actual)).reset_index()

        # Recorrer arreglo de programas y agregar cada fila con graficos
        cont = 0
        for i,p in enumerate(list_programas):
            # Filtrar por programa 
            data = df_IES[df_IES['programa']==p].reset_index()
            
            # Filtrar por periodo actual
            data = data.sort_values(by=['periodo']).reset_index()
            data_periodo_index = data.query('periodo == {}'.format(periodo_actual))

            # Verificar si existe registro para ese periodo
            if len(data_periodo_index) > 0:
                periodo_index = data_periodo_index.index[0]

                # Verificar si existe dato en el periodo anterior
                if periodo_index-1 in data['index']:
                    periodo_anterior = data.loc[periodo_index-1,'periodo']
                    # Obtener registros de los dos ultimos periodos a partir del indicado 
                    data_periodo_index = data.query('periodo == {} | periodo == {}'.format(periodo_actual,periodo_anterior))
                    fig = indicadores_programa_row(data,fig,cont+1)
                    cont += 1

        # Personalizar la grafica
        fig.update_layout(
            paper_bgcolor = '#fff',
            plot_bgcolor = '#fff',
            showlegend=False,
            margin=dict(l=0, r=0, t=30, b=0,pad=0),
        )
        for annotation in fig['layout']['annotations']: 
            annotation['font'] = dict(color='black')

        return fig,cant_programas

    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None


############################################################### LISTA MINI SERIES PROGRAMAS ####################################################################
# Funcion para agregar un registro de programas a la lista de graficos
def miniserie_programa_row(data,fig,i, dict_periodos):
    x_periodos = []
    for p in data['periodo']:
        x_periodos.append(dict_periodos[p])

    periodos = [str(p) for p in data['periodo']]

    # Mini Serie de tiempo
    fig.append_trace(go.Scatter(
        x=x_periodos, 
        y=data['desercion'],
        mode='lines+markers',
        marker=dict(
            color='rgb(0, 94, 235)',
        ),
        text = periodos,
        hovertemplate='<br>(%{text} , %{y:.1%}) <extra></extra>',
        ),row=i,col=1)
    fig.update_yaxes(
        showline=False,
        tickvals= [statistics.mean(data['desercion'])],
        ticktext = ['<b>'+data['programa_nombre_corto'][0]+'</b>       '],
        tickfont=dict(
            color='black', 
            # size=15
        ),
        fixedrange=True,
        row=i, col=1
        ) 
    fig.update_xaxes(
        showline=False,
        showticklabels=False,
        title="",
        fixedrange=True,
        range=[-1, 20],
        row=i, col=1
    )

    # Indicador 
    periodo_anterior = data.loc[len(data)-2,'desercion']
    periodo_actual = data.loc[len(data)-1,'desercion']
    
    if periodo_actual == periodo_anterior:
        periodo_actual = None
        periodo_anterior = None
    
    if periodo_anterior == 0:
        periodo_anterior = 1
        periodo_actual = 2

    fig.append_trace(go.Indicator(
        mode = "delta",
        delta = {'reference': periodo_anterior,'relative': True, 'position' : "bottom"},
        value = periodo_actual,
        ),
        row=i,col=2
    )

    return fig

def miniseries_programas():
    try:
        # Programas
        data = df_IES.dropna(subset=['facultad'], axis=0)
        list_programas = data['programa'].unique()
        cant_programas = len(list_programas)

        # Periodos
        list_periodos = sorted(df_IES['periodo'].unique())
        cant_periodos = len(list_periodos)
        dict_periodos = {}
        for i,p in enumerate(list_periodos):
            dict_periodos[p]=i

        # Retencion

        # Crear conetenedor de sub graficos
        fig = make_subplots(
            start_cell='top-left',
            column_titles=['<b>Desercion / Semestre</b>', ''],
            rows=cant_programas, cols=2,
            column_widths=[0.5, 0.1],
            specs=[[{"type": "scatter"}, {"type": "indicator"}]]*cant_programas,
        )

        # Recorrer arreglo de programas y agregar cada fila con graficos
        for i,p in enumerate(list_programas):
            data = df_IES[df_IES['programa']==p]
            data = data[['periodo','programa','programa_nombre_corto','desercion']]
            data  =  data.dropna().reset_index()
            # data['periodo'] = data['periodo'].astype(str)
            fig = miniserie_programa_row(data,fig,i+1,dict_periodos)

        # Personalizar la grafica
        fig.update_layout(
            paper_bgcolor = '#fff',
            plot_bgcolor = '#fff',
            showlegend=False,
            margin=dict(l=0, r=0, t=30, b=0,pad=0),
        )
        for annotation in fig['layout']['annotations']: 
                    annotation['font'] = dict(color='black')

        return fig, cant_programas

    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None

############################################################### PASTEL ####################################################################
def pastel(periodo):
    try:
        periodo_actual = int(periodo)

        # Agrupar por Facultades
        data_facultades = df_IES.query('periodo == {}'.format(periodo_actual))
        data_facultades = data_facultades.dropna(subset=['facultad'], axis=0)

        data_facultades= data_facultades[['facultad','programa','programa_nombre_corto','mat_total','admi_total','insc_total','mat_nuevos_total']]
        data_facultades = data_facultades.groupby(['facultad'],as_index=False).sum()
        data_facultades =  data_facultades.sort_values(by=['admi_total','insc_total','mat_nuevos_total'], ascending=[True,True,True])

        fig = px.pie(
            values=data_facultades['mat_total'],
            names=data_facultades['facultad'],
            color_discrete_sequence=px.colors.sequential.ice[3:],
            hole=.3
            )
        # Personalizar la grafica
        fig.update_layout(
            paper_bgcolor = '#fff',
            plot_bgcolor = '#fff',
            # showlegend=True,
            showlegend=False,
            margin=dict(l=0, r=0, t=0, b=5,pad=0),
        )
        fig.update_traces(
            hovertemplate= '<b>%{label}</b>'+
                '<br>%{value} <extra></extra>',
            marker=dict(line=dict(color='white', width=2))
            # hoverlabel = dict(bgcolor='white'),
        )
        for annotation in fig['layout']['annotations']: 
            annotation['font'] = dict(color='black')
        return fig
    except Exception as e:
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        return None