import pyodbc
from dotenv import load_dotenv
import os,sys

load_dotenv()

_db = os.getenv("CLI_DB_NAME")
_host = os.getenv("CLI_DB_SERVER")
_user = os.getenv("CLI_DB_USER")
_password = os.getenv("CLI_DB_PASSWORD")

class DB:
    def __init__(self):
        self.connect()

    def connect(self):
        try:
            self.cnx = pyodbc.connect(
                "Driver={ODBC Driver 17 for SQL SERVER};"
                "Server="+_host+";"
                "Database="+_db+";"
                "UID="+_user+";"
                "PWD="+_password+";"
            )
            self.exception = None
        except Exception as e:
            self.cnx = None
            self.exception = e

    def select(self,sql):
        try:
            if self.cnx is None:
                self.connect()
                return self.exception
            cur = self.cnx.cursor()
            cur.execute(sql)
            cur.close
        except Exception as e:
            self.connect()
            return e  

        lista = []
        rows = cur.fetchall()
        columns = [i[0] for i in cur.description]
        for row in rows:
            # Create a zip object from two lists
            registro = dict(zip(columns, row))
            # Create a dictionary from zip object
            json = dict(registro)
            lista.append(json)
        
        self.cnx.close
        return lista

    def insert(self,body,tabla):
        # Keys Body
        columns = str(tuple(body.keys())).replace("'", "")
        # Values Body
        values = str((tuple(body.values())))
        # Sentencia SQL
        sql = "INSERT INTO {} {} VALUES{}".format(tabla, columns, values).replace('None','NULL')
        return self.execute(sql)

    def update(self,body,condicion,tabla):
        set_values = str(body)[2:-1].replace("':"," =").replace(", '",", ")
        sql = "UPDATE {} SET {} WHERE {};".format(tabla, set_values, condicion).replace('None','NULL')
        return self.execute(sql) 

    def delete(self,condicion,tabla):
        # Sentencia SQL
        sql = "DELETE FROM {} WHERE {};".format(tabla, condicion)
        return self.execute(sql)

    def execute(self,sql):
        # Manejar error de ejecucion
        try:
            if self.cnx is None:
                self.connect()
                return self.exception
            cur = self.cnx.cursor()
            cur.execute(sql)
            self.cnx.commit()
            cur.close
            self.cnx.close
            return cur
        except Exception as e:
            self.connect()
            return e  

