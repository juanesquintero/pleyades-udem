import mysql.connector as mysql
from dotenv import load_dotenv
import os
from functools import wraps

load_dotenv()

_host = os.getenv("DB_HOST")
_user = os.getenv("DB_USER")
_password = os.getenv("DB_PASSWORD")
_database = os.getenv("DB_NAME")


def validate_connection(f):
    @wraps(f)
    def decorated_function(self,*args, **kwargs):
        if not(self.cnx):
            self.connect()
            raise Exception('Conexion caida!')
        else:
            try:
                return f(self, *args, **kwargs)
            except:
                self.close_connection()
                self.connect()
                raise Exception('Conexion caida!')
    return decorated_function


class DB:
    __instance = None
    
    @staticmethod
    def getInstance():
        if DB.__instance == None:
            DB()
        return DB.__instance
    
    def __init__(self):
        if DB.__instance != None:
            raise Exception("Esta clase es un Singleton!")
        else:
            self.cnx = None
            self.connect()          
            DB.__instance = self
    
    def connect(self):
        try:
            self.cnx = mysql.MySQLConnection(
                host=_host,
                port=3306,
                user=_user,
                password=_password,
                database=_database,
            )
        except:
            self.close_connection()
            
    def close_connection(self):
        if self.cnx:
            if self.cnx.is_connected():
                connection.close()
        self.cnx = None

    def execute(self,sql):
        # Manejar error de ejecucion
        try:
            if self.cnx is None:
                self.connect()
                return self.exception
            cur = self.cnx.cursor()
            cur.execute(sql)
            self.cnx.commit()
            cur.close
            self.cnx.close
            return cur
        except Exception as e:
            self.connect()
            return e  

    @validate_connection
    def insert(self,body,tabla):
        # Keys Body
        columns = str(tuple(body.keys())).replace("'", "`")
        # Values Body
        values = str(tuple(body.values()))
        # Sentencia SQL
        sql = "INSERT INTO {} {} VALUES{}".format(tabla, columns, values).replace('None','NULL')
        return self.execute(sql) 

    @validate_connection
    def select(self,sql):
        try:
            if self.cnx is None:
                self.connect()
                return self.exception
            cur = self.cnx.cursor()
            cur.execute(sql)
        except Exception as e:
            self.connect()
            return e  
        lista = []
        rows = cur.fetchall()
        columns = [i[0] for i in cur.description]
        for row in rows:
            # Create a zip object from two lists
            registro = dict(zip(columns, row))
            # Create a dictionary from zip object
            json = dict(registro)
            lista.append(json)
        cur.close
        self.cnx.close
        return lista

    @validate_connection
    def update(self,body,condicion,tabla):
        # Sentencia SQL
        set_values = '`'+str(body)[2:-1].replace("':","`=").replace(", '",", `")
        sql = "UPDATE {} SET {} WHERE {};".format(tabla, set_values, condicion).replace('None','NULL')
        return self.execute(sql) 

    @validate_connection
    def delete(self,condicion,tabla):
        # Sentencia SQL
        sql = "DELETE FROM {} WHERE {};".format(tabla, condicion)
        return self.execute(sql)