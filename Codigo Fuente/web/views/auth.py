from flask import session, request, redirect, Blueprint, render_template
from functools import wraps
import os, dotenv, requests, jwt

dotenv.load_dotenv()

api = os.getenv("API_PATH")

Auth = Blueprint('auth', __name__)

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'user' in session.keys():
            return render_template('utils/login.html'), 200
        if session['user'] is None:
            return render_template('utils/login.html'), 200
        return f(*args, **kwargs)
    return decorated_function

def logout_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'user' in session.keys():
            return f(*args, **kwargs)
        if session['user'] is None:
            return f(*args, **kwargs)
        return render_template('utils/inicio.html'), 200
    return decorated_function

def only_admin(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not 'user' in session.keys():
            return render_template('utils/login.html'), 200
        if session['user'] is None:
            return render_template('utils/login.html'), 200
        if session['user']['rol']=='Admin':
            return f(*args, **kwargs)
        return render_template('utils/mensaje.html', mensaje="Usted no tiene autorizacion para realizar esta accion"), 401
    return decorated_function

@Auth.route('/login',methods=['GET','POST'])
@logout_required
def login():
    if request.method == 'GET':
        return render_template('utils/login.html'), 200

    # Loguear
    raw_usuario = dict(request.values)
    usuario = { 'correo': raw_usuario['InputEmailLogin'], 'clave': raw_usuario['InputPasswordLogin']}
    res = requests.post(api+'auth/login',json=usuario)
    status,body = res.status_code, res.json()
    if status==200:
        session['user'] = None
        session['headers'] = None
        # Obtener token
        token = body['access_token']
        # Setear usuario de la session
        user = jwt.decode(token,os.getenv('JWT_KEY'))['identity']
        session['user'] = user
        # Definir objeto request para realizar peticiones  al API 
        session['headers'] = {'Authorization': 'Bearer ' + token}        
        session.permanent = True

        return render_template('utils/inicio.html'), 200
    if 'msg' in body.keys():
        return render_template('utils/login.html',mensaje=body['msg']), 401
    return render_template('utils/error.html', mensaje="Ocurrió un error iniciando sesión", submensaje=body['error']), 400
    
    
@Auth.route('/logout')
@login_required
def logout():
    session['user'] = None
    session['headers'] = None
    session.clear()
    return render_template('utils/login.html'), 200


